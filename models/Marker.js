"use strict";

module.exports = function (sequelize, DataTypes) {
    var Marker = sequelize.define('marker', {
        label: {
            type: DataTypes.STRING
        },
        desc: {
            type: DataTypes.STRING
        },
        lat: {
            type: DataTypes.DOUBLE
        },
        lng: {
            type: DataTypes.DOUBLE
        }
    });
    
    return Marker;
};