var express = require('express');
var router = express.Router();
var models = require('../models')
var cors = require('cors')
var markerController = require('../controllers/markerController')

router.use(cors());


/* GET home page. */
router.get('/', function (req, res, next) {
});

router.get('/get-markers', function (req, res, next) {
    markerController.list(req, res, next);
});

router.post('/add-marker', function (req, res, next) {
    markerController.add(req, res, next);
});

router.delete('/delete-marker/:marker_id', function (req, res, next) {
    markerController.remove(req, res, next);
});

module.exports = router;
