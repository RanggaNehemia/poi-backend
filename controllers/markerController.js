var models = require('../models');

module.exports = {
    list: function (req, res, next) {
        models.marker.findAll().then(function (data) {
            res.json(data);
        });
    },

    add: function (req, res, next) {
        models.marker.create({
            label: req.body.label,
            desc: req.body.desc,
            lat: req.body.lat,
            lng: req.body.lng
        }).then(function (data) {
            res.json(data);
        });
    },

    remove: function (req, res, next) {
        models.marker.destroy({
            where: {
                id: req.params.marker_id
            }
        }).then(function (data) {
            res.json(data);
        });
    }
};